import 'package:dartz/dartz.dart';
import 'package:dui/features/login/domain/entities/UserFacebook.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/LoginRepository.dart';
import 'package:dui/features/login/domain/usecases/LoginWithFacebook.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockLoginRepository extends Mock implements LoginRepository {}

void main() {
  LoginWithFacebook usecase;
  MockLoginRepository mockLoginRepository;

  setUp(() {
    mockLoginRepository = MockLoginRepository();
    usecase = LoginWithFacebook(mockLoginRepository);
  });

  final tName = 'Miguel';
  final tLastname = 'Vilcapuma';
  final tEmail = 'lvilcapumas8@gmail.com';
  final UserFacebook userFacebook = UserFacebook(
    name: tName,
    lastname: tLastname,
    email: tEmail,
  );

  test(
    'deberia poder logearse con facebook',
    () async {
      //arrange
      when(mockLoginRepository.loginWithFacebook())
          .thenAnswer((_) async => Right(userFacebook));

      //act
      final result = await usecase(NoParams());

      //assert
      expect(result, Right(userFacebook));
    },
  );
}
