import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/RegisterRepository.dart';
import 'package:dui/features/login/domain/usecases/Register.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRegisterRepository extends Mock implements RegisterRepository {}

void main() {
  Register usecase;
  MockRegisterRepository mockRegisterRepository;

  setUp(() {
    mockRegisterRepository = MockRegisterRepository();
    usecase = Register(mockRegisterRepository);
  });

  final tName = 'miguel';
  final tLastname = 'vilcapuma';
  final tEmail = 'lvilcapumas8@gmail.com';
  final UserRegistered userRegister = UserRegistered(
    name: tLastname,
    lastname: tLastname,
    email: tEmail,
  );

  test(
    'test - debería porder registrar un usuario',
    () async {
      //arrange
      when(mockRegisterRepository.register(
              name: tName, lastname: tLastname, email: tEmail))
          .thenAnswer((_) async => Right(userRegister));

      //act
      final result = await usecase(Params(
        name: tName,
        lastname: tLastname,
        email: tEmail,
      ));

      //assert
      expect(result, Right(userRegister));
      verify(mockRegisterRepository.register(
          name: tName, lastname: tLastname, email: tEmail));
      verifyNoMoreInteractions(mockRegisterRepository);
    },
  );
}
