import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/RegisterRepository.dart';
import 'package:dui/features/login/domain/usecases/RegisterWithFacebook.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRegisterWithFacebook extends Mock implements RegisterRepository {}

void main() {
  RegisterWithFacebook usecase;
  MockRegisterWithFacebook mockRegisterWithFacebook;

  setUp(() {
    mockRegisterWithFacebook = MockRegisterWithFacebook();
    usecase = RegisterWithFacebook(mockRegisterWithFacebook);
  });

  final tName = 'miguel';
  final tLastname = 'vilcapuma';
  final tEmail = 'lvilcapumas8@gmail.com';
  final UserRegistered userRegister = UserRegistered(
    name: tName,
    lastname: tLastname,
    email: tEmail,
  );

  test(
    'test - debería poder registrar usuario con facebook',
    () async {
      //arrange
      when(mockRegisterWithFacebook.registerWithFacebook())
          .thenAnswer((_) async => Right(userRegister));

      //act
      final result = await usecase(NoParams());

      //assert
      expect(result, Right(userRegister));
      verify(mockRegisterWithFacebook.registerWithFacebook());
      verifyNoMoreInteractions(mockRegisterWithFacebook);
    },
  );
}
