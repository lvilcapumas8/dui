import 'package:dartz/dartz.dart';
import 'package:dui/core/errors/Failure.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/LoginRepository.dart';
import 'package:dui/features/login/domain/usecases/LoginUser.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockLoginRepository extends Mock implements LoginRepository {}

void main() {
  LoginUser usecase;
  MockLoginRepository mockLoginRepository;

  setUp(() {
    mockLoginRepository = MockLoginRepository();
    usecase = LoginUser(mockLoginRepository);
  });

  final tEmail = 'lvilcapumas8@gmail.com';
  final tName = 'Miguel';
  final tLastname = 'Vilcapuma';
  final tPassword = 'secret';
  final UserRegistered userRegistered =
      UserRegistered(name: tName, lastname: tLastname, email: tEmail);
  test(
    'debería poder logearse satisfactoriamente',
    () async {
      //arrange
      when(mockLoginRepository.login(email: tEmail, password: tPassword))
          .thenAnswer((_) async => Right(userRegistered));

      //act
      final result = await usecase(Params(email: tEmail, password: tPassword));

      //assert
      expect(result, Right(userRegistered));
      verify(mockLoginRepository.login(email: tEmail, password: tPassword));
    },
  );
}
