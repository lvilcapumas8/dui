import 'dart:convert';
import 'package:dui/features/login/data/models/UserModel.dart';
import 'package:dui/features/login/domain/entities/User.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final UserModel userModel = UserModel(
    email: 'lvilcapumas8@gmail.com',
    name: 'Miguel',
    lastname: 'Vilcapuma',
  );

  test('deberia ser subclase de User', () async {
    expect(userModel, isA<User>());
  });

  test(
    'debería retornar un modelo al ingresar un json',
    () async {
      //arrange
      final Map<String, dynamic> jsonMap = json.decode(fixture('user.json'));

      //act
      final result = UserModel.fromJson(jsonMap);

      //assert
      expect(result, equals(userModel));
    },
  );

  test(
    'deberia retornar la data del json',
    () async {
      //arrange
      final expected = {
        "name": "Miguel",
        "lastname": "Vilcapuma",
        "email": "lvilcapumas8@gmail.com",
      };

      final result = userModel.toJson();
      //act

      //assert
      expect(expected, equals(result));
    },
  );
}
