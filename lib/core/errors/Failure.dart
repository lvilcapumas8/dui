import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  var properties;

  Failure([List properties = const <dynamic>[]]);

  @override
  List<Object> get props => properties;
}

//Failures general

class CacheFailure extends Failure {}

class ServerFailure extends Failure {}

class FacebookFailure extends Failure {}

class LoginFailure extends Failure {}
