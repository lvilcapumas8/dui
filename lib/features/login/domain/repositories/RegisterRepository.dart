import 'package:dui/core/errors/Failure.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

abstract class RegisterRepository {
  Future<Either<Failure, UserRegistered>> register({
    @required String name,
    @required String lastname,
    @required String email,
  });

  Future<Either<Failure, UserRegistered>> registerWithFacebook();
}
