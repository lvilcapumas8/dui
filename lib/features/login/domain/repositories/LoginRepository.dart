import 'package:dui/core/errors/Failure.dart';
import 'package:dartz/dartz.dart';
import 'package:dui/features/login/domain/entities/UserFacebook.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:flutter/cupertino.dart';

abstract class LoginRepository {
  Future<Either<Failure, UserRegistered>> login(
      {@required String email, @required dynamic password});

  Future<Either<Failure, UserFacebook>> loginWithFacebook();
}
