import 'package:dui/core/errors/Failure.dart';
import 'package:dui/core/usecases/UseCase.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/RegisterRepository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class RegisterWithFacebook implements UseCase<UserRegistered, NoParams> {
  final RegisterRepository registerRepository;

  RegisterWithFacebook(this.registerRepository);

  @override
  Future<Either<Failure, UserRegistered>> call(NoParams params) async {
    return await registerRepository.registerWithFacebook();
  }
}

class NoParams extends Equatable {
  @override
  List<Object> get props => null;
}
