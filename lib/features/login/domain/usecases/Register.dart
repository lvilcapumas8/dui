import 'package:dui/core/errors/Failure.dart';
import 'package:dui/core/usecases/UseCase.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/RegisterRepository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class Register implements UseCase<UserRegistered, Params> {
  final RegisterRepository repository;

  Register(this.repository);

  @override
  Future<Either<Failure, UserRegistered>> call(Params params) async {
    return await repository.register(
        name: params.name, lastname: params.lastname, email: params.email);
  }
}

class Params extends Equatable {
  final String name;
  final String lastname;
  final String email;

  Params({this.name, this.lastname, this.email});

  @override
  List<Object> get props => null;
}
