import 'package:dartz/dartz.dart';
import 'package:dui/core/errors/Failure.dart';
import 'package:dui/core/usecases/UseCase.dart';
import 'package:dui/features/login/domain/entities/UserRegistered.dart';
import 'package:dui/features/login/domain/repositories/LoginRepository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class LoginUser implements UseCase<UserRegistered, Params> {
  final LoginRepository loginRepository;

  LoginUser(this.loginRepository);

  @override
  Future<Either<Failure, UserRegistered>> call(Params params) async {
    return await loginRepository.login(
        email: params.email, password: params.password);
  }
}

class Params extends Equatable {
  final String email;
  final password;

  Params({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}
