import 'package:dartz/dartz.dart';
import 'package:dui/core/errors/Failure.dart';
import 'package:dui/core/usecases/UseCase.dart';
import 'package:dui/features/login/domain/entities/UserFacebook.dart';
import 'package:dui/features/login/domain/repositories/LoginRepository.dart';
import 'package:equatable/equatable.dart';

class LoginWithFacebook implements UseCase<UserFacebook, NoParams> {
  LoginRepository loginRepository;

  LoginWithFacebook(this.loginRepository);

  @override
  Future<Either<Failure, UserFacebook>> call(NoParams params) async {
    return await loginRepository.loginWithFacebook();
  }
}

class NoParams extends Equatable {
  @override
  List<Object> get props => null;
}
