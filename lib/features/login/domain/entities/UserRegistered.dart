import 'package:dui/features/login/domain/entities/User.dart';

class UserRegistered extends User {
  UserRegistered({name, lastname, email})
      : super(name: name, lastname: lastname, email: email);
}
