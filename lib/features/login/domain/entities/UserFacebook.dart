import 'package:dui/features/login/domain/entities/User.dart';
import 'package:flutter/cupertino.dart';

class UserFacebook extends User {
  UserFacebook({name, lastname, email})
      : super(name: name, lastname: lastname, email: email);
}
