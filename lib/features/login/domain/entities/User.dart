import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class User extends Equatable {
  final String name;
  final String lastname;
  final String email;

  User({@required this.name, @required this.lastname, @required this.email});

  @override
  List<Object> get props => [name, lastname, email];
}
