import 'package:dui/features/login/domain/entities/User.dart';
import 'package:flutter/cupertino.dart';

class UserModel extends User {
  UserModel({
    @required String name,
    @required String lastname,
    @required String email,
  }) : super(
          name: name,
          lastname: lastname,
          email: email,
        );

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      name: json['name'],
      lastname: json['lastname'],
      email: json['email'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'lastname': lastname,
      'email': email,
    };
  }
}
