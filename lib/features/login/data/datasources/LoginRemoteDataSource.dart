import 'dart:convert';

import 'package:dui/features/login/data/models/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

abstract class LoginRemoteDataSource {
  Future<UserModel> login(String email, String password);

  Future<UserModel> loginWithFacebook();

  correr();
}

class LoginRemoteDataSourceImpl implements LoginRemoteDataSource {
  final http.Client client;

  LoginRemoteDataSourceImpl({@required this.client});

  @override
  correr() {
    return 'miguel';
  }

  @override
  Future<UserModel> login(String email, String password) async {
    final response = await client.post(
      'url',
      headers: {'Content-Type': 'application/json'},
      body: {"email": email, "password": password},
    );

    return UserModel.fromJson(json.decode(response.body));
  }

  @override
  Future<UserModel> loginWithFacebook() {
    return null;
  }
}
