import 'package:dui/features/login/data/datasources/LoginRemoteDataSource.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final service = GetIt.instance;

Future<void> init() async {
  service.registerLazySingleton<LoginRemoteDataSource>(
      () => LoginRemoteDataSourceImpl(client: service()));

  service.registerLazySingleton(() => http.Client());
}

void go() {
  final datasource = service<LoginRemoteDataSource>();

  print(datasource.correr());
}
